package myconn

import (
	"database/sql"
	"net"

	"github.com/go-sql-driver/mysql"
)

type (
	MySqlConn struct {
		DBName string
		User   string
		Passwd string
		Net    string
		Host   string
		Port   string
		Params map[string]string
	}
)

func New(dbName, dbUser, dbPass string) *MySqlConn {
	return &MySqlConn{
		DBName: dbName,
		User:   dbUser,
		Passwd: dbPass,
		Net:    "tcp",
		Host:   "localhost",
		Port:   "3306",
		Params: map[string]string{
			"parseTime": "true",
			"loc":       "Local",
		},
	}
}

func MySql(conn *MySqlConn) (*sql.DB, error) {
	c := mysql.NewConfig()
	c.DBName = conn.DBName
	c.User = conn.User
	c.Passwd = conn.Passwd
	c.Net = conn.Net
	c.Addr = net.JoinHostPort(conn.Host, conn.Port)
	c.Params = conn.Params
	return sql.Open("mysql", c.FormatDSN())
}
